﻿using Microsoft.EntityFrameworkCore;
using Autofac;

ContainerBuilder builder = new ContainerBuilder();

// Đăng ký AppDbContext với DbContextOptions
builder.Register(c =>
{
    // Tạo options từ DbContextOptionsBuilder
    DbContextOptionsBuilder<AppDbContext> optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
    optionsBuilder.UseInMemoryDatabase("testDatabase"); // Có thể thay đổi bằng UseSQL...
    return optionsBuilder.Options;
}).As<DbContextOptions<AppDbContext>>();

// Đăng ký AppDbContext
builder.RegisterType<AppDbContext>();

// Tạo Autofac Container từ ContainerBuilder
IContainer container = builder.Build();

// Tự động khởi tạo object có kiểu là AppDbContext
AppDbContext context = container.Resolve<AppDbContext>();

// Thêm 1 người
context.Persons.Add(new Person { Name = "hahaha " });
context.SaveChanges();

List<Person> persons = context.Persons.ToList();
Console.WriteLine();

public class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

    public DbSet<Person> Persons { get; set; }
}

public class Person
{
    public int Id { get; set; }
    public string Name { get; set; }
}